package com.home.montyhall.model;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by salae on 2016-10-15.
 */
@Entity
public class SimulationResult extends AbstractId {

    @Temporal(TemporalType.TIMESTAMP)
    private final Date creationTime = new Date();
    private int numberOfGames;
    private GameStrategy strategy;
    private Long wins = Long.valueOf(0);
    private Long loses = Long.valueOf(0);

    public SimulationResult() {
    }

    public SimulationResult(int numberOfGames, GameStrategy strategy) {
        this.numberOfGames = numberOfGames;
        this.strategy = strategy;
    }

    public Long getWins() {
        return wins;
    }

    public void setWins(Long wins) {
        this.wins = wins;
    }

    public Long getLoses() {
        return loses;
    }

    public void setLoses(Long loses) {
        this.loses = loses;
    }

    public GameStrategy getStrategy() {
        return strategy;
    }
}
