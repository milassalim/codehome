package com.home.montyhall.model;

/**
 * Created by salae on 2016-10-15.
 */
public enum GameStrategy {
    KEEP,
    SWITCH
}
