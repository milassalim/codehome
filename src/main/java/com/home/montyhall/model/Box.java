package com.home.montyhall.model;

/**
 * Created by salae on 2016-10-15.
 */

//@Entity
public class Box {

    private BoxStatus status = BoxStatus.CLOSED;

    private BoxContent content;

    public Box(BoxContent content) {
        this.content = content;
    }

    public BoxStatus getStatus() {
        return status;
    }

    public void setStatus(BoxStatus status) {
        this.status = status;
    }

    public BoxContent getContent() {
        return content;
    }
}
