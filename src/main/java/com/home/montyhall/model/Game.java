package com.home.montyhall.model;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by salae on 2016-10-15.
 */

public class Game {

    private static final Random RANDOM = new Random();
    private List<Box> boxes;
    private GameStatus status;
    private GameStrategy strategy = GameStrategy.KEEP;

    public Game() {
        this.boxes = setBoxes();
    }

    private List<Box> setBoxes() {
        int winningBox = RANDOM.nextInt(3);

        return IntStream.range(0, 3).mapToObj(index -> {
            BoxContent content = (index == winningBox) ? BoxContent.MONEY : BoxContent.EMPTY;
            return new Box(content);
        }).collect(Collectors.toList());
    }

    public List<Box> getBoxes() {
        return boxes;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public GameStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(GameStrategy strategy) {
        this.strategy = strategy;
    }

}
