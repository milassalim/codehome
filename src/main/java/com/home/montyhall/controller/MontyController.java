package com.home.montyhall.controller;

import com.home.montyhall.model.Game;
import com.home.montyhall.model.GameStrategy;
import com.home.montyhall.model.SimulationResult;
import com.home.montyhall.service.SimulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by salae on 2016-10-15.
 */
@Controller
public class MontyController {

    @Autowired
    SimulationService simulationService;

    @RequestMapping(value = "/simulation", method = RequestMethod.GET)
    @ResponseBody
    public SimulationResult runSimulation(@RequestParam("games") int games,
                                          @RequestParam("strategy") GameStrategy strategy) {

        SimulationResult simulation = simulationService.createSimulation(games, strategy);
        return simulationService.saveSimulation(simulation);

    }

    @ExceptionHandler({IllegalArgumentException.class, ConversionFailedException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleException(IllegalArgumentException exception) {
        return exception.getMessage().concat("Request failed");
    }

    @RequestMapping(value = "/game", method = RequestMethod.GET)
    @ResponseBody
    public Game getGame() {
        return new Game();
    }
}
