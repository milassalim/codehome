package com.home.montyhall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeHomeApplication.class, args);
	}
}
