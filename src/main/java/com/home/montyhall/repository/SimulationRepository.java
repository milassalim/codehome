package com.home.montyhall.repository;

import com.home.montyhall.model.SimulationResult;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * Created by salae on 2016-10-15.
 */

public interface SimulationRepository extends CrudRepository<SimulationResult, Long> {
    @RestResource(path = "/history")
    List<SimulationResult> findTop10ByOrderByCreationTimeDesc();
}
