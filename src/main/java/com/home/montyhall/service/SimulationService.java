package com.home.montyhall.service;

import com.home.montyhall.model.*;
import com.home.montyhall.repository.SimulationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by salae on 2016-10-15.
 */
@Service
public class SimulationService {

    @Autowired
    SimulationRepository simulationRepository;

    public static void selectAbox(Game game) {
        int random = new Random().nextInt(3);
        game.getBoxes().get(random).setStatus(BoxStatus.SELECTED);
    }

    public static void openOneEmptyBox(Game game) {
        final boolean[] opened = new boolean[1];
        opened[0] = false;
        game.getBoxes().forEach(box -> {
            if (box.getStatus() == BoxStatus.CLOSED
                    && box.getContent() == BoxContent.EMPTY
                    && !opened[0]) {
                box.setStatus(BoxStatus.OPEN);
                opened[0] = true;
            }
        });

    }

    public static void switchBoxes(Game game) {
        game.getBoxes().forEach(box -> {
            if (box.getStatus() == BoxStatus.SELECTED) {
                box.setStatus(BoxStatus.CLOSED);
            } else if (box.getStatus() == BoxStatus.CLOSED) {
                box.setStatus(BoxStatus.SELECTED);
            }
        });

    }

    public SimulationResult createSimulation(int games, GameStrategy strategy) {

        SimulationResult simulation = new SimulationResult(games, strategy);

        Map<GameStatus, Long> result = IntStream.range(0, games)
                .mapToObj(i -> strategy)
                .map(gameStrategy -> simulate(gameStrategy))
                .collect(
                        Collectors.groupingBy(val -> val, Collectors.counting()));

        simulation.setLoses(result.get(GameStatus.LOSE));
        simulation.setWins(result.get(GameStatus.WIN));

        return simulation;
    }

    public SimulationResult saveSimulation(SimulationResult simulationResult) {
        return simulationRepository.save(simulationResult);
    }

    public GameStatus simulate(GameStrategy strategy) {
        //Set the game with 3 boxes
        Game game = new Game();

        //Select a Box
        selectAbox(game);

        //Open an empty Box
        openOneEmptyBox(game);

        //Switch or Not
        if (strategy == GameStrategy.SWITCH) {
            switchBoxes(game);
        }

        //Reveil the game
        BoxContent content = game.getBoxes().stream().filter(
                box -> {
                    return box.getStatus() == BoxStatus.SELECTED;
                }
        ).findFirst().get().getContent();

        return content == BoxContent.MONEY ? GameStatus.WIN : GameStatus.LOSE;

    }

}
