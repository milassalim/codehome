var app = angular.module('app', ['chart.js']);

app.controller('gameController', function ($scope, $rootScope, $http, $log) {

    $rootScope.game = 'new';
    $scope.getGame = function () {
        $http({
            method: 'GET',
            url: './game'
        }).success(function (data) {
            $scope.boxes = data.boxes;
            $rootScope.state = 'game'
            $rootScope.game = 'started';
        }).error(function (data, status) {
            $log.error(status);
        });
    };

    $scope.startSimulation = function () {
        $rootScope.state = "simulation";
        $rootScope.game='simulating';
    }

    $scope.clickBox = function (box) {
        //if box open do nothing
        if (box.status === 'OPEN') return;

        //Don't want to switch ... end game
        if (box.status === 'SELECTED') {
            endGame(box);
            return
        }

        //Select or switch box
        if (!($scope.boxes.find(selectedBox))) {
            box.status = 'SELECTED';
            $scope.gameStatus === 'selected';
            $rootScope.game = 'selected';

            //open an empty box
            openEmptyBox();
        } else {
            switchBox(box);
        }
    };

    selectedBox = function (box) {
        return box.status === 'SELECTED';
    }

    openEmptyBox = function () {
        var open = false;
        while (!open) {
            var randomBox = $scope.boxes[Math.floor(Math.random() * $scope.boxes.length)];
            if (randomBox.status === 'CLOSED' && randomBox.content != 'MONEY') {
                randomBox.status = 'OPEN'
                open = true;
            }
        }
        $rootScope.game = 'switch';
    };

    switchBox = function (box) {
        $scope.boxes.forEach(function (box) {
            if (box.status === 'SELECTED') {
                box.status = 'CLOSED'
            } else if (box.status === 'CLOSED') {
                box.status = 'SELECTED'
            }
        });
        endGame(box);
    };

    endGame = function (box) {
        $scope.boxes.forEach(function (abox) {
            abox.status = 'OPEN';
        });
        if (box.content === 'MONEY') {
            $rootScope.game = 'won';
        } else {
            $rootScope.game = 'lost';
        }
    }

});

app.controller('simulationController', function ($scope, $rootScope, $http, $log) {

    $scope.goToGame = function () {
        $rootScope.state = 'game';
        $rootScope.game = 'new';
    };

    $scope.runSimulation = function () {
        if ( !$scope.games || ! $scope.strategy) return;

        var labels = ['wins', 'loses']
        $scope.data = {};
        $scope.labels = {};
        $scope.series = {}
        $scope.labels.simulation = labels;
        $scope.labels.history = [];
        $scope.data.simulation = [];
        $scope.data.history = [];
        $scope.series.history = labels;

        $scope.options = {
            responsive: false,
            maintainAspectRatio: false
        };
        $scope.colors = ["#beefd2","#ffddfb"]
        $http({
            method: 'GET',
            url: './simulation?games=' + $scope.games + '&strategy=' + $scope.strategy
        }).success(function (data) {
            $scope.data.simulation = $scope.labels.simulation.map(function (key) {
                return data[key];
            });
        }).error(function (data, status) {
            $log.error(status);
        }).then(
            $http({
                method: 'GET',
                url: './simulationResults/search/history'
            }).success(function (data) {
                wins = [];
                loses = []
                var int = 0;
                $scope.series.history = $scope.labels.simulation
                angular.forEach(data._embedded.simulationResults, function (data) {
                    wins.push(data[$scope.series.history[0]]);
                    loses.push(data[$scope.series.history[1]]);
                    $scope.labels.history.push(data['strategy'])
                });
                console.log()
                $scope.data.history = [wins, loses]
            }).error(function (data, status) {
                $log.error(status);
            })
        );
    }
})



