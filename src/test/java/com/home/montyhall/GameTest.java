package com.home.montyhall;

/**
 * Created by salae on 2016-10-15.
 */

import com.home.montyhall.model.*;
import com.home.montyhall.model.Box;
import com.home.montyhall.model.Game;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


public class GameTest {

    @Test
    public void createGame() {
        Game game = new Game();
        List<Box> boxes= game.getBoxes();

        assertThat(boxes, is(Matchers.<Box>iterableWithSize(3)));

        assertTrue(boxes.stream().anyMatch(item -> "CLOSED".equals(item.getStatus().toString())));

    }

}
