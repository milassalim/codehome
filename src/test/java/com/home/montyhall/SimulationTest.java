package com.home.montyhall;

/**
 * Created by salae on 2016-10-15.
 */

import com.home.montyhall.model.GameStrategy;
import com.home.montyhall.model.SimulationResult;
import com.home.montyhall.repository.SimulationRepository;
import com.home.montyhall.service.SimulationService;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimulationTest {

    @Autowired
    SimulationRepository simulationRepository;

    @Autowired
    SimulationService simulationService;

    @Test
    public void testSimulations() {
        SimulationResult simulation = simulationService.createSimulation(1, GameStrategy.KEEP);
        SimulationResult saved = simulationRepository.save(simulation);

        Iterable<SimulationResult> sims = simulationRepository.findAll();
        assertThat(sims, is(Matchers.<SimulationResult>iterableWithSize(1)));

    }

}
