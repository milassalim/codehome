# README #
Coding challenge: Montyhall problem solution writen in java and javascript. 
Used Spring Boot, Spring Data, Rest, angularjs and chart.js

## Quick summary ##
* Java backend to create game sessions, save simulations and serve games and simulation history through RESTservices

### Prerequisites###
- Java 8
- Maven 3+
- Modern web browser (HTML5)

### Guide ###
- Clone project or download source code locally
- run maven command 

```
#!maven
mvn clean package spring-boot:run
```
Open your browser (Not Internet Explorer please!) at 

```
#!HTTP

http://localhost:8888/montyhall
```
* A start page with choice:
- Emulation of the Box game.
- A simulator to run n games with the selected strategy (switch or not).
The result is shown in a doughnut chart, and the history of up to 10 last simulations is displayed in a bar chart.



### Showcase ###
- Java 8 features
- Spring boot
- Spring Data JPA, Spring Data Rest
- Rest api
- Web application, angularjs, bootstrap, chart.js
- In-memory database